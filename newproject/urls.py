from django.contrib import admin
from django.urls import path

from forum import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', views.home, name='home'),
    path('<int:pk>/', views.board, name='board'),
    path('<int:pk>/new', views.new_topic, name='new_topic'),
    path('<int:pk>/<int:topic_id>/', views.topic, name='topic'),
]
