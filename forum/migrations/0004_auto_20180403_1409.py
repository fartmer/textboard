# Generated by Django 2.0.3 on 2018-04-03 11:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0003_auto_20180403_1212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='created_by',
            new_name='nickname',
        ),
    ]
