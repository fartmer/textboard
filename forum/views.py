from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect
from .forms import NewTopicForm, NewCommentForm

from .models import Board, Topic, Comment

def home(request):
    boards = Board.objects.all()
    return render(request, 'forum/home.html', {'boards':boards})

def board(request, pk):
    board = get_object_or_404(Board, pk=pk)
    return render(request, 'forum/board.html', {'board':board})

def new_topic(request, pk):
    board = get_object_or_404(Board, pk=pk)

    if request.method == 'POST':
        form = NewTopicForm(request.POST)
        if form.is_valid():
            topic = form.save(commit=False)
            topic.board = board
            topic.save()            
            return redirect('board', pk=board.pk)

    else:
        form = NewTopicForm()
    return render(request, 'forum/new_topic.html', {'board':board, 'form':form})

def topic(request, pk, topic_id):
    topic = get_object_or_404(Topic, pk=topic_id)
    
    if request.method == 'POST':
        form = NewCommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.topic = topic
            comment.save()            
            return redirect('topic', pk=pk, topic_id=topic_id)
    else:
        form = NewCommentForm()
    return render(request, 'forum/topic.html', {'topic':topic, 'form':form})