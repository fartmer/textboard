from django import forms

from .models import Topic, Comment

class NewTopicForm(forms.ModelForm):
  class Meta:
    model = Topic
    fields = ['nickname','title', 'subject']

class NewCommentForm(forms.ModelForm):
  class Meta:
    model = Comment
    fields = ['nickname', 'message']
    widgets = {'message': forms.Textarea(attrs={'rows':4}),}


        