from django.db import models
from django.contrib.auth.models import User

class Board(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def num_topics(self):
        return self.topics.count()

    def num_comments(self):
        return sum([t.num_comments() for t in self.topics.all()])



class Topic(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE, related_name='topics')
    title = models.CharField(max_length=100)
    subject = models.TextField(max_length=2000)
    created_at = models.DateTimeField(auto_now_add=True)
    nickname = models.CharField(max_length=100)

    def num_comments(self):
        return self.comments.count()

    def __str__(self):
        return u"%s - %s - %s" % (self.board, self.nickname, self.title)

    class Meta:
        ordering = ['-created_at']

class Comment(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='comments')
    nickname = models.CharField(max_length=100)
    message = models.CharField(max_length=1000)

    def __str__(self):
        return u"%s - %s" %(self.topic, self.nickname)